package de.mathesnet.rpg.models;

import java.util.Map;

public class Equipment extends Item {
	public Equipment(int itemId, String itemName, double itemPrice) {
		super(itemId, itemName, itemPrice);
	}
}
