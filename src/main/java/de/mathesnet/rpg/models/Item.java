package de.mathesnet.rpg.models;

import java.util.HashMap;
import java.util.Map;

public class Item {
	private int itemId;
	private String itemName;
	private double itemPrice;
	private boolean isCraftable;
	private Map<Item,Integer> craftComponentsByNumber;

	public Item(int itemId, String itemName, double itemPrice) {
		this.itemId = itemId;
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.isCraftable = false;
		this.craftComponentsByNumber = new HashMap<>();
	}

	public void addCraftingRequirement(Item item, Integer number) {
		this.isCraftable = true;
		craftComponentsByNumber.put(item, number);
	}

	public boolean isCraftable() {
		return isCraftable;
	}

	@Override
	public String toString() {
		return "Item{" +
			  "isCraftable=" + isCraftable +
			  ", itemPrice=" + itemPrice +
			  ", itemName='" + itemName + '\'' +
			  ", itemId=" + itemId +
			  '}';
	}
}
