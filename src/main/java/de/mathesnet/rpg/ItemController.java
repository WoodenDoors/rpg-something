package de.mathesnet.rpg;

import de.mathesnet.rpg.models.CraftingComponent;
import de.mathesnet.rpg.models.Equipment;
import de.mathesnet.rpg.models.Item;
import de.mathesnet.rpg.models.Usable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ItemController {
	private List<Item> equipment;
	private List<Item> usableItems;
	private List<Item> craftingComponents;
	private List<Item> craftables;
	private Random random;

	public ItemController(){
		equipment = new ArrayList<>();
		usableItems = new ArrayList<>();
		craftingComponents = new ArrayList<>();
		craftables = new ArrayList<>();
		random = new Random();

		addAlItems();
	}

	/**
	 * maybe read this from file?
	 */
	public void addAlItems(){
		equipment.add(new Equipment(1, "Schwert", 500));
		equipment.add(new Equipment(1, "Helm", 500));
		equipment.add(new Equipment(1, "magischer Nasenring", 10));
		equipment.add(new Equipment(1, "Schild", 200));

		Item hop = new CraftingComponent(1, "Hopfen", 10);
		craftingComponents.add(hop);

		Item beer = new Usable(1, "kleines Bier", 500);
		beer.addCraftingRequirement(hop, 3);
		usableItems.add(beer);
		craftables.add(beer);
	}

	/**
	 * generate random item
	 */
	public Item generateItem(){
		List<Item> listToChooseFrom = chooseList();
		if(listToChooseFrom == null){
			return null;
		}

		int position = random.nextInt(listToChooseFrom.size());
		return listToChooseFrom.get(position);
	}

	/**
	 * helper for {@link #generateItem()}
	 *
	 * TODO: change from numbers to ranges to influence probability
	 */
	private List<Item> chooseList(){
		List<Item> listToChooseFrom;

		int listNum = random.nextInt(3);
		switch(listNum){
			case 0:
				listToChooseFrom = equipment;
				break;
			case 1:
				listToChooseFrom = usableItems;
				break;
			default:
				listToChooseFrom = craftingComponents;
		}

		return listToChooseFrom;
	}
}
