package de.mathesnet.rpg;

import de.mathesnet.rpg.models.CraftingComponent;
import de.mathesnet.rpg.models.Equipment;
import de.mathesnet.rpg.models.Item;
import de.mathesnet.rpg.models.Usable;
import org.junit.Before;
import org.junit.Assert;
import org.junit.Test;

public class ItemControllerTest {
	private ItemController controller;

	@Before
	public void setUp(){
		controller = new ItemController();
	}

	@Test
	public void testGenerateItem(){
		int number = 100;
		boolean oneEquipment = false;
		boolean oneUsable = false;
		boolean oneCraftingComponent = false;
		boolean oneCraftable = false;

		for(int i=0; i<number; i++){
			Item item = controller.generateItem();
			if(item instanceof Equipment) {
				oneEquipment = true;
			}
			if(item instanceof Usable) {
				oneUsable = true;
			}
			if(item instanceof CraftingComponent) {
				oneCraftingComponent = true;
			}
			if(item.isCraftable()) {
				oneCraftable = true;
			}
			System.out.println(item);
		}
		Assert.assertTrue(oneEquipment);
		Assert.assertTrue(oneUsable);
		Assert.assertTrue(oneCraftingComponent);
		Assert.assertTrue(oneCraftable);
	}
}